from abc import ABC, abstractmethod
from random import random, randint, choice


class Unit(ABC):
    _name = None
    _strength = None
    _health = None
    _double_damage_coef = None
    _defence = None

    def __init__(self, name, strength, health, double_damage_coef, defence):
        self._name = self._check_name(name)
        self._strength = strength
        self._health = health
        self._double_damage_coef = self._check_damage_coef(double_damage_coef)
        self._defence = self._check_defence(defence)

    def attack(self, enemy):
        if not isinstance(enemy, Unit):
            raise Exception
        return self._attack(enemy)

    @abstractmethod
    def _attack(self, enemy):
        '''if enemy == evasion:...
        check for a class with 10% chance of evasion'''
        pass

    def _double_damage(self, enemy):
        '''2x damage function:
        r_damage = randomizer'''
        def_coef = (100 - enemy._defence) * 0.01
        if r_damage <= self._double_damage_coef:
            enemy._health -= self._strength * 2 * def_coef
            return enemy._health
        else:
            enemy._health -= self._strength * def_coef
            return enemy._health

    @staticmethod
    def _check_name(val):
        if not isinstance(val, str) and len(val) != 0:
            raise Exception
        return val

    @staticmethod
    def _check_damage_coef(val):
        if not isinstance(val, (int, float)):
            raise Exception
        return val

    @staticmethod
    def _check_defence(val):
        if not isinstance(val, (int, float)):
            raise Exception
        return val

    @property
    def strength(self):
        if self._strength <= 0:
            self._strength = 0
        return self._strength

    @strength.setter
    def strength(self, val):
        if not isinstance(val, float):
            raise Exception
        if val <= 0:
            self._strength = 0
        self._strength = val

    @property
    def health(self):
        if self._health <= 0:
            self._health = 0
        return self._health

    @health.setter
    def health(self, val):
        if not isinstance(val, float):
            raise Exception
        self._health = val


class Minister(Unit):
    '''+10% of self.health'''
    def _attack(self, enemy):
        if enemy == evasion:
            if r_evasion > 10:
                self._double_damage(enemy)
                self._health += self._strength * 0.1
        else:
            self._double_damage(enemy)
            self._health += self._strength * 0.1

    def __str__(self):
        return '{}'.format(self._name)


class Deputy(Unit):
    '''-17% of enemy.health'''
    def _attack(self, enemy):
        if enemy == evasion:
            if r_evasion > 10:
                self._double_damage(enemy) * 1.7
                self._health += self._strength
        else:
            self._double_damage(enemy) * 1.7
            self._health += self._strength

    def __str__(self):
        return '{}'.format(self._name)


class Judge(Unit):
    '''default settings'''
    def _attack(self, enemy):
        if enemy == evasion:
            if r_evasion > 10:
                self._double_damage(enemy)
                self._health += self._strength
            else:
                self._double_damage(enemy)
                self._health += self._strength

    def __str__(self):
        return '{}'.format(self._name)


class President(Unit):
    '''10% chance of evasion'''
    def _attack(self, enemy):
        if enemy == evasion:
            if r_evasion > 10:
                self._double_damage(enemy)
            else:
                self._double_damage(enemy)

    def __str__(self):
        return '{}'.format(self._name)


# 2. Для Игры: напишите класс Battle, в котором присутствует метод fight.
# Метод принимает два юнита, случайным образом выбирает первого и атакует
# первым юнитом второго. Потом вторым атакует первого и т.д. пока один из
# юнитов не погибнет


class Battle:

    _unit1 = None
    _unit2 = None
    _unit3 = None
    _unit4 = None

    def __init__(self, unit1, unit2, unit3, unit4):
        self._unit1 = unit1
        self._unit2 = unit2
        self._unit3 = unit3
        self._unit4 = unit4

    @staticmethod
    def _fight(unit1, unit2, unit3, unit4):
        fight_unit1 = choice([unit1, unit2, unit3, unit4])
        fight_unit2 = choice([unit1, unit2, unit3, unit4])
        while fight_unit1 == fight_unit2:
            fight_unit2 = choice([unit1, unit2, unit3, unit4])
        print(fight_unit1, fight_unit1.health, fight_unit2, fight_unit2.health)

        if r_kill == 0:
            fight_unit2._health -= fight_unit2._health
            print(fight_unit2, fight_unit2._health)

        while fight_unit1.health != 0 and fight_unit2.health != 0:
            print(str(fight_unit1), fight_unit1.health)
            print(str(fight_unit2), fight_unit2.health)
            fight_unit1.attack(fight_unit2)
            fight_unit2.attack(fight_unit1)
            print(str(fight_unit1), fight_unit1.health)
            print(str(fight_unit2), fight_unit2.health)
        if fight_unit1.health == 0 and fight_unit2.health == 0:
            return 'Both {} and {} are dead'.format(fight_unit1, fight_unit2)
        elif fight_unit1.health == 0:
            return 'The winner is {}'.format(fight_unit2)
        else:
            return 'The winner is {}'.format(fight_unit1)


minister = Minister(name='Zlyden', strength=100, health=1000, double_damage_coef=0.15, defence=10)
deputy = Deputy(name='Vral', strength=200, health=2000, double_damage_coef=0.51, defence=20)
judge = Judge(name='Hapuga', strength=300, health=3000, double_damage_coef=0.37, defence=30)
president = President(name='Zelo', strength=400, health=4000, double_damage_coef=0.99, defence=40)
evasion = President
r_kill = randint(0, 1)
print('kill', r_kill)
r_evasion = randint(0, 100)
print('evasion', r_evasion)
r_damage = random()
print('damage', r_damage)

battle = Battle(minister, deputy, judge, president)
fight = battle._fight(minister, deputy, judge, president)
print(fight)
